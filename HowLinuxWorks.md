#How Linux Works
## A short description for non-linux users
###Level: intermediate
This document is intended to provide a swift introduction  for non-linux user

## What is this about

This tutorial is not "about linux" but also about how computer works with linux.
This is not an "indeep" explanation but just an introduction in the wonderfull world of linux and open source.

- - -

## What is linux ? What is a boot loader ? What is a linux distribution ? What is opensource ?

linux is a "little" program that make other software able to use your computer. This means "it is a kernel".

As name suggest, a kernel is the core of an operating system. His job is to provide functions that can be used by other programs to perform some functions (like displaying images, memory management and reading / writing to all sort of memory ).
Basically it accomplish this job by some software components called drivers which are in fact little piece of software that know what a specific part of hardware is able to do.

Different kernels implements different ways to talk to drivers and also how functions are exposed to user software. This is called API in both ways - the API for driver communication and the API with exposed functions.
The critical part is that all components interact so if something is wrong somewhere, the failure must be somehow isolated and the entire system must recover from error without little or no user intervention. This is part of "system stability".

Also, the core system has to implement some function that restrict user rights to perform some critical actions or affect other user work. This is part of "security" of the system and also part of "multi user environment".

If the linux is the kernel - core of the operating system, there is also a program that load linux - the boot loader.
In fact when computer start it load his BIOS program (which is burned in computer hardware) that make some hardware checks and load the bootloader  from specific location. The job of bootloader is to identify and launch the kernel of operating system. After the kernel its launched, the control of computer is taken by the kernel which load itself and start the operating system. This mean launching a chain of programs that perform some generic operations for all systems in specific mode and ends with the launch of user interface.

Those above are true for all operating systems, even is terminal based (keyboard based interface ) or GUI based (keyboard an mouse intensive use ) and also for all computer hardware systems with some variants.

A linux distribution is a system that allow us to install all components above and help us to install and maintain a linux-based operating system. It consist in an install medium and install system, a boot loader, a linux kernel version, and a packaging system that allow user to customize after install what it will use. For all this to work the installer first deploy a "base system" which is the core of the distribution and can be thought about as minimal software components that make your linux distribution work.
Distributions share most of software but differ in the way they use-it. Some linux distribution use modified version of linux kernel to reach some specific goals or different version of packages. That make any distribution different some how. Also, packaging system can be related but different from one distribution to another. That does not mean it is another OS. Any distribution is... linux.


So all distribution use some ,if not all, common software. Where did this software came from ?

Most of this software is open source. This mean, some people thought it is interesting to do something and also it is nice to share it with you. This is because in this approach anyone can see the source of software and improve-it. Also there are some nice thoughts about freedom and kindness that made the use of this programs available for you.

So ... there are developers that maintain some open source programs, packers that make distributions for you. Even linux is open source,and most of available boot loaders.

Ok... but how they got payed for they work ? 

There are many payment models (have you heard about bitcoin ? is open source too :D ) but this are the most used models for 
making money from open source:
-donation - you can donate if you like
-reputation - you can rent a coder with good open source reputation to work with
-payed support - you can buy support to quick fix some configuration or bugs ;). this not affect quality of the software.


There are also non-free components, most from hardware vendors protecting some designs (binary firmwares like for wi-fi cards) or from commercial products (Adobe Flash Development Tools for example ). And there also many ways to do same task using different software.





### How linux (open source, etc) is different from others (Mac, Win, other opensource systems ) :

There are plenty of differences and similar things in software world.

The paradox of linux is that you can`t even compare two linux distribution. "Are the same" is both true and false. It is true because all are linux and open source, is false because each distribution is personalized to achieve a specific goal.


Also linux distribution is not the only open source based operating system. It share some parts(most drivers ) with other unix-based operating systems, but implementation can differ. Also, other systems have internal kernel architecture different to achive different goals. So there is FreeBSD, OpenBSD, NetBSD, DragonFly BSD, Haiku OS , etc and distribution based on this. The main real difference is the underlying filesystem that you can use for install on, beside code or internal architecture, and even the fact that in theory you can install linux on HFS or FreeBSD on ext_x. If this difference was gone, only the internal architecture will make the difference and because all are open source, it is very hard to differentiate between which architecture is the best.And when i said architecture i mean not only the API exposed but the way parts of kernel interact each other. In fact linux and other opensource based os`s are so intimate that in the wild are projects that share same packaging system - look at Debian BSD projects or similar.

With the biggest numbers of developers involved, if a serious problem occur, it is likely to be fixed in hours.This make linux and opensource programs almost immune to "viruses". 
Because opensource is open, there are organisations that help so it is likely to find a mirror near you that provide linux distro or open source software, so you will need to compile from source only if you want to.
For some reasons (citation needed) the community of programmers/users involved in  BSD or other open source based kernel/systems is not so big that those available for linux. I think this is because the distribution system is not as mature as linux  , there are less number of mirrors available - in a phrase, they marketing sucks.


Compared to Apple OS , opensource based operating systems... are open. Apple OS works on a limited range of hardware, and at core, Apple OS s are... opensource based. In fact, Mac OS X and iOS are Darwin based (the open source Mac OS part )( - and Darwin share most of his code with FreeBSD. Not all, but enough to be considered a FreeBSD derivate. We talk here about the kernel (and driver ) parts. Mac OS share also the file system architecture, and some packages. But expose a different, simplified and stable,closed source API to the applications.

Because it is not entirely opensource, a problem is fixed in more time than in opensource software. Luckly it is available only to limited range of hardware so most of possible problems are solved.

Things are different with Win based os s. The source of this OS is available only for a limited number of persons, so it is difficult to fix bugs. And this is the main problem of OS. This lead consumers to suspect Microsoft for all evil things in the world. But also, because the huge numbers of users, the hardware manufacturers provide almost always drivers for those operating systems. Also, there is a problem with the fact that you can use Microsoft products only a limited period of time.
They don't provide support for none of operating systems before Windows 7 at this time(ok... Vista is supported ? ) and also no support for other old versions of products.unless you are a VIC (very important customer ). More, if you have an old computer (let s say you have a functional high quality old drive ) you can`t use-it with MS technology. And this because you could not change anything inside the os.

With opensource based os, you can use another version of kernel that suit your old hardware. It is true, new hardware can sometime be unusable until someone release a driver (i was in that situation with intel+nvidia composite video driver), but always there will be a solution. 

You can see some frustration about opensource here http://security.stackexchange.com/questions/4441/open-source-vs-closed-source-systems




###But wait, there's more...

Most of devices at your home use linux based operating systems. That means it is very likely that your tv, your "hardware" router or your house automation is linux based.If you use and Android phone, there is nothing more to say... Wait it is... 
there is in the wild many Android and Android -like distributions, some not just clones but bringing innovations - Take a look at Chrome OS or Firefox OS.

And this
http://techluminati.com/operating-systems/linux-vs-windows/